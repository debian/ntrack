
/* Generated data (by glib-mkenums) */

#ifndef __NTRACK_ENUM_TYPES_H__
#define __NTRACK_ENUM_TYPES_H__

#include <glib-object.h>

G_BEGIN_DECLS

/* enumerations from "../glib/ntrack-enums.h" */
GType n_track_glib_state_get_type (void) G_GNUC_CONST;
#define N_TYPE_TRACK_GLIB_STATE (n_track_glib_state_get_type ())
GType n_track_glib_event_get_type (void) G_GNUC_CONST;
#define N_TYPE_TRACK_GLIB_EVENT (n_track_glib_event_get_type ())
G_END_DECLS

#endif /* __GIO_ENUM_TYPES_H__ */

/* Generated data ends here */

